<?php

require('animal.php');
require('frog.php');
require('ape.php');

$sheep = new animal("shaun");

echo "Name : " .  $sheep->name . "<br>";
echo "legs : " .  $sheep->leg . "<br>";
echo "cold blooded : " .  $sheep->cold . "<br> <br>" ;

$ape = new ape("Ape");

echo "Name : " .  $ape->name . "<br>";
echo "legs : " .  $ape->leg . "<br>";
echo "cold blooded : " .  $ape->cold . " <br>";
echo "Yell : " . $ape->yell() . "<br> <br>";

$frog = new frog("Frog");

echo "Name : " .  $frog->name . "<br>";
echo "legs : " .  $frog->leg . "<br>";
echo "cold blooded : " .  $frog->cold . "<br>";
echo "Jump : " . $frog->jump()

?>