<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('halaman.register');
    }

    public function kirim(Request $request)
    {
        $namaawal = $request['awal'];
        $namaakhir = $request['akhir'];

        return view('halaman.home',  ['namaawal' => $namaawal, 'namaakhir' => $namaakhir]);


    }
}


